<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Firebase\JWT\JWT;

class User extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'person_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function generateToken()
    {
        $accessToken = JWT::encode(array(
            'identifier' => $this->id
        ), config('auth.access_token_key'));

        return $accessToken;
    }

    public static function checkToken(Request $request)
    {
        // get token from request
        $token = $request->headers->get('authorization') ?
            $request->headers->get('authorization') : $request->get('authorization');

        // get jwt
        $jwt = trim(str_replace('bearer', '', str_replace('Bearer', '', $token)));

        // decode jwt
        try {
            $decoded = JWT::decode($jwt, config('auth.access_token_key'), array('HS256'));

            // find user
            if (User::find($decoded->identifier)->first()) {
                return true;
            }
        } catch (\Exception $e) {
            //
        }

        return false;
    }
}
