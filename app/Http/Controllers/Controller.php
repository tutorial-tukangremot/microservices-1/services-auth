<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class Controller extends BaseController
{
    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|exists:users,email',
            'password' => 'required'
        ]);

        $user = User::where('email', $request->input('email'))->first();

        if ($user && Hash::check($request->input('password'), $user->password)) {
            return response()->json([
                'success' => true,
                'message' => __('auth.logged_in'),
                'data' => [
                    'access_token' => $user->generateToken(),
                    'token_type' => 'Bearer',
                    'person_id' => $user->person_id
                ]
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => __('auth.account_does_not_exist')
        ], 401);
    }

    public function introspect(Request $request)
    {
        if (User::checkToken($request)) {
            return response()->json([
                'success' => true,
                'message' => 'Valid'
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Invalid'
        ], 401);
    }
}
