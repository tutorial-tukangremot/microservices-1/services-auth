<?php

namespace Tests;

use App\Models\User;

class RouteTest extends TestCase
{
    public function testLogin()
    {
        /**
         * start test with empty data
         */
        $this->post('/auth/login');

        $this->seeStatusCode(422);
        $this->seeJsonStructure([
            'success',
            'message',
            'data' => [
                'fields' => [
                    'email',
                    'password'
                ]
            ]
        ]);
        /**
         * end test with empty data
         */

        /**
         * start test with valid data
         */

        // create fake user
        $user = User::factory()->create([
            'email' => $this->faker->email
        ]);

        $this->post('/auth/login', [
            'email' => $user->email,
            'password' => 'test'
        ]);

        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'success',
            'message',
            'data' => [
                'access_token',
                'token_type',
                'person_id'
            ]
        ]);
        /**
         * end test with valid data
         */

        /**
         * start test with invalid data
         */
        $this->post('/auth/login', [
            'email' => $user->email,
            'password' => 'testing' // invalid password
        ]);

        $this->seeStatusCode(401);
        $this->seeJson([
            'success' => false,
            'message' => __('auth.account_does_not_exist')
        ]);
        /**
         * end test with invalid data
         */
    }

    public function testIntrospect()
    {
        /**
         * start test with invalid/empty data
         */
        $this->post('/auth/introspect');

        $this->seeStatusCode(401);
        $this->seeJson([
            'success' => false,
            'message' => 'Invalid'
        ]);
        /**
         * end test with invalid/empty data
         */


        /**
         * start test with valid data
         */

        // fake login to get token
        $user = User::factory()->create([
            'email' => $this->faker->email
        ]);

        $this->post('/auth/login', [
            'email' => $user->email,
            'password' => 'test'
        ]);
        $access_token = $this->response->decodeResponseJson()['data']['access_token'];

        // start test
        $this->post('/auth/introspect', [], ['HTTP_Authorization' => 'Bearer ' . $access_token]);

        $this->seeStatusCode(200);
        $this->seeJson([
            'success' => true,
            'message' => 'Valid'
        ]);
        /**
         * end test with valid data
         */
    }
}
