<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            $user = User::create([
                'email' => 'tukangremot@example.com',
                'person_id' => 1,
                'password' => Hash::make('test')
            ]);
        } catch (\Throwable $th) {
            //
        }
    }
}
