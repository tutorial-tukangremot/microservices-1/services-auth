# Services Auth
* [Request & Response Examples](#request--response-examples)

## Request & Response Examples
### API Resources

* Login
    * [POST /auth/login](#post-login)
* Introspect
    * [POST /auth/introspect](#post-introspect)

### POST /auth/login

Request body:

    {
        "email": "tukangremot@example.com",
        "password": "test"
    }

Response body:

    {
        "success": true,
        "message": "Login successfully",
        "data": {
            "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZGVudGlmaWVyIjoxfQ.yj90hfEWqEaNT1WXXMLfiFVqqN3RY1-aKxkq-9R9T8w",
            "token_type": "Bearer",
            "person_id": "1"
        }
    }
### POST /auth/introspect

Response body:

    {
        "success": true,
        "message": "Valid"
    }
